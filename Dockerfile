FROM node:16 as node
WORKDIR /app
COPY . .
RUN npm ci -f
RUN npm run build
