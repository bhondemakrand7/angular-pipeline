## [1.2.0](https://gitlab.com/bhondemakrand7/angular-pipeline/compare/1.1.1...1.2.0) (2022-10-21)


### Features

* **core:** added releaserc ([feffa3f](https://gitlab.com/bhondemakrand7/angular-pipeline/commit/feffa3f322cf763683e132e7c4e3c3d9dd8d9a49))
* **core:** del releaserc ([761fc4d](https://gitlab.com/bhondemakrand7/angular-pipeline/commit/761fc4d7baa77086f088326109d8d8ccf605e81a))

## [1.1.1](https://gitlab.com/bhondemakrand7/angular-pipeline/compare/1.1.0...1.1.1) (2022-10-21)


### Performance Improvements

* **core:** signup api rewritten ([02ce795](https://gitlab.com/bhondemakrand7/angular-pipeline/commit/02ce7954b8dfab59108db23c659eb1c7d3b1bed7))

## [1.1.0](https://gitlab.com/bhondemakrand7/angular-pipeline/compare/1.0.1...1.1.0) (2022-10-21)


### Features

* **core:** added feature B ([2a0169d](https://gitlab.com/bhondemakrand7/angular-pipeline/commit/2a0169d8afddd546737eb851fb99d5542ee3b22d))

## [1.0.1](https://gitlab.com/bhondemakrand7/angular-pipeline/compare/1.0.0...1.0.1) (2022-10-20)


### Bug Fixes

* **pencil:** title change ([df57c13](https://gitlab.com/bhondemakrand7/angular-pipeline/commit/df57c13a479fd7c59c3915bad07c9ff196ddf877))

## [1.0.0](https://gitlab.com/bhondemakrand7/angular-pipeline/compare/...1.0.0) (2022-10-20)


### Bug Fixes

* **yml:** yaml conf changed ([87b22dc](https://gitlab.com/bhondemakrand7/angular-pipeline/commit/87b22dceff762ab5ef9876d216446a20d9909bcd))
